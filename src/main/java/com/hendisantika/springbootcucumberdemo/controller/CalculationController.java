package com.hendisantika.springbootcucumberdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-cucumber-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/08/18
 * Time: 07.27
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("calc")
public class CalculationController {
    @GetMapping("/add/{num1}/{num2}")
    public String add(@PathVariable int num1, @PathVariable int num2) {
        return "Addition of " + num1 + " + " + num2 + " is " + (num1 + num2);
    }

    @GetMapping("/sub/{num1}/{num2}")
    public String subtract(@PathVariable int num1, @PathVariable int num2) {
        return "Subtraction of " + num1 + " - " + num2 + " is " + (num1 - num2);
    }

    @GetMapping("/multiply/{num1}/{num2}")
    public String multiple(@PathVariable int num1, @PathVariable int num2) {
        return "Multiple of " + num1 + " * " + num2 + " is " + (num1 * num2);
    }

    @GetMapping("/divide/{num1}/{num2}")
    public String divide(@PathVariable int num1, @PathVariable int num2) {
        return "Divide of " + num1 + " / " + num2 + " is " + (num1 / num2);
    }
}
